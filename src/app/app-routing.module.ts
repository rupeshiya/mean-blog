import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AddPostComponent } from './add-post/add-post.component';
import { EditPostComponent } from './edit-post/edit-post.component';
import { AllPostsComponent } from './all-posts/all-posts.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ProfileComponent } from './profile/profile.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UpdatePasswordComponent } from './update-password/update-password';
import { NewsletterComponent } from './newsletter/newsletter.component';

const routes: Routes = [
    {  path: 'signup', component: SignupComponent },
    {  path: 'login', component: LoginComponent },
    {  path: 'addPost', component: AddPostComponent },
    {  path: 'editPost/:id', component: EditPostComponent },
    {  path:  'allPosts', component: AllPostsComponent },
    {  path:  'editProfile/:id', component: EditProfileComponent },
    {  path:  'profile', component: ProfileComponent },
    {  path: 'reset-password', component: ResetPasswordComponent },
    {  path: 'update-password', component: UpdatePasswordComponent },
    {  path: 'newsletter', component: NewsletterComponent },
    {  path:  ' ', redirectTo: 'allPosts' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
