import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../services/auth-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
// import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
title: any;
content: any;
  constructor(
    private router: Router,
    private authService: AuthServiceService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  // on add post click
  onAddPostClick() {
    const data = {
      title: this.title,
      body: this.content
    };
    if (this.validatPost()) {
      this.authService.createPost(data).subscribe((res) => {
        if (res.success) {
            console.log('created the post');
            this.flashMessage.show('Successfully created the post !!', {cssClass: 'alert-success', timeout: 3000});
            this.router.navigate(['/allPosts']);
        } else {
            this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
        }
      });
    }
  }

  // validation of the form
  validatPost(): boolean {
    if (this.title === undefined) {
      this.flashMessage.show('Please enter the title !!', {cssClass: 'alert-danger', timeout: 2000});
      return false;
    } else if (this.content === undefined) {
      this.flashMessage.show('Please enter the secription of the title !!', {cssClass: 'alert-danger', timeout: 2000});
      return false;
    }
    return true;
  }
}
