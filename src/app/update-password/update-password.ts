import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthServiceService } from '../services/auth-service.service';
// import { url } from 'inspector';

@Component({
  selector: 'app-enter-new-password',
  templateUrl: './update-password.html',
  styleUrls: ['./update-password.css']
})
export class UpdatePasswordComponent implements OnInit {
username: any;
password: any;
confirmPassword: any;
url: any;

  constructor(
    private router: Router,
    private flashMessage: FlashMessagesService,
    private authService: AuthServiceService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    // this.route.url.subscribe((url)=>{
    //   if(url.includes('users/')){
    //     this.router.navigate(['/update-password']);
    //   } else {
    //     console.log('error in navigating to update -password url !!');
    //   }
    // })
    this.url = window.location.href.split('?')[0];
    if (this.url === 'http://localhost:4000/users/reset') {
      this.router.navigate(['/update-password']);
    } else {
      console.log('url doesn\'t match');
    }
  }

  // on done submit
  onSubmitClick() {
    if (this.matchPassword) {
      // update password and then redirect to login
      const newPassword = {
        username: this.username,
        newPassword : this.password
      };
      this.authService.updatePassword(newPassword).subscribe((res) => {
        if (res.success) {
          console.log('update the password !!');
        } else {
          console.log('error in updating password !!');
        }
      });
      this.router.navigate(['/login']);
    } else {
      this.flashMessage.show('Password does not match !! ', {cssClass: 'alert-danger', timeout: 2000});
    }
  }

  // match password
  matchPassword(): boolean {
    if (this.confirmPassword === this.password) {
      return true;
    } else {
      return false;
    }
  }


}
