import { Component, OnInit } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { AuthServiceService } from '../services/auth-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
password: string;
email: string;
username: string;
isChecked: Boolean;
snackBarDuration = 2000;

  constructor(
    private authService: AuthServiceService,
    private flashMessage: FlashMessagesService,
    private router: Router,
    private MatSnackBar: MatSnackBar
    ) { }

  ngOnInit() {
    this.checked();
  }

  // onLogin click
  onLoginClick() {
    const data = {
      username: this.username,
      email: this.email,
      password: this.password
    };
    if (this.validateForm()) {
      this.authService.authenticateUser(data).subscribe((res) => {
        console.log('response -', res);
        if (res.success) {
          this.authService.storeUserData(res.token, res.user);
          console.log('login the user !!');
          this.flashMessage.show('Successfully loggedIn !!', {cssClass: 'alert-success', timeout: 3000});
          // this.MatSnackBar.open('Successfully loggedIn !!',null,{
          //   duration: this.snackBarDuration });
          this.router.navigate(['/allPosts']);
        } else {
          this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
        }
      });
    } else {
      this.flashMessage.show('error !!', {cssClass: 'alert-danger', timeout: 3000});
    }
  }

  // validate Login Form
  validateForm(): boolean {
    if (this.username === undefined) {
      this.flashMessage.show('Please enter the username !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    } else if (this.email === undefined) {
      this.flashMessage.show('Please enter the email !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    } else if (this.password === undefined) {
      this.flashMessage.show('Please enter the password !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    }
    return true;
  }

  // functio to add remember me functionalities
  RememberMe() {
    const checkbox = document.getElementById('exampleCheck1') as HTMLInputElement;
    this.isChecked = checkbox.checked;
    console.log('remember me clicked !!');
    this.checked();
  }

  // function to get checked
  checked() {
    if (this.isChecked) {
      console.log('remember me checked !!');
      const username = JSON.parse(localStorage.getItem('user')).username;
      const email =  JSON.parse(localStorage.getItem('user')).email;
      const password =  JSON.parse(localStorage.getItem('user')).password;
      this.username = username;
      this.email = email;
      this.password = password;
      console.log('username-', username);
      console.log('email-', email);
      console.log('password-', password);
    } else {
      console.log('not remember me checked !!');
    }
  }

  // to navigate to the reset-password view
  onForgotClick() {
    this.router.navigate(['/reset-password']);
  }

}
