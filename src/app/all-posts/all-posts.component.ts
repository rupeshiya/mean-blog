import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PageEvent } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.css']
})
export class AllPostsComponent implements OnInit {
posts: any;
pageLength = 10;
pageSizeOptions = [1, 3, 5, 7, 10];
pageSize = 3;
currentPage = 1;
comment = false;
commentBody: any;
totalLikes: any;
totalDislike: any;

  constructor(
    private authService: AuthServiceService,
    private flashMessage: FlashMessagesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getPosts();
  }

  // functio to get all the posts
  getPosts() {
    this.authService.getPosts(this.pageSize, this.currentPage).subscribe((data) => {
      if (data.success) {
        console.log('posts data from component -', data);
        this.posts = data.posts;
      } else {
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }


  // on edit click
  onEditClick(id) {
    this.router.navigate([`/editPost/${id}`]);
  }

  // on delete click
  onDeleteClick(id) {
    this.authService.deletePostById(id).subscribe((res) => {
      if (res.success) {
        this.flashMessage.show('Successfully deleted post !!', {cssClass: 'alert-success', timeout: 3000});
        this.router.navigate(['/']);
      } else {
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-success', timeout: 3000});
      }
    });
  }

  // for toggling comment
  onCommentClickToggle() {
    this.comment = !this.comment;
  }

  // for adding comment
  onDoneComment(id) {
    const data = {
      commentBody: this.commentBody
    };
    this.authService.addComment(id, data).subscribe((res) => {
      if (res.success) {
        this.comment = false;
        console.log('Successfully added the comment !!');
        this.flashMessage.show('Sucessfully added the comment!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-success', timeout: 3000});
      }
    });
  }

  onSubscribeClick() {
    this.router.navigate(['/newsletter']);
  }

  // function to add likes and dislikes
  onThumbsUpClick(id) {
    this.authService.likes(id).subscribe((res) => {
      if (res.success) {
        this.totalLikes = res.totalUpVote;
        this.totalDislike = res.totalDownVote;
        console.log('Liked the posts !!');
        this.flashMessage.show('Thanks for liking the post !!', {cssClass: 'alert-success', timeout: 2000 });
      } else {
        console.log('Something went wrong in liking the posts !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-success', timeout: 2000});
      }
    });
  }

  onThumbsDownClick(id) {
    this.authService.dislikes(id).subscribe((res) => {
      if (res.success) {
        this.totalLikes = res.totalUpVote;
        this.totalDislike = res.totalDownVote;
        console.log('Dislikes the posts !!');
        this.flashMessage.show('Opps !! , sorry for the post !!', {cssClass: 'alert-success', timeout: 2000 });
      } else {
        console.log('Something went wrong !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-success', timeout: 2000});
      }
    });
  }

  // function for paginations
  onChangedPage(pageData: PageEvent) {
    // adding 1 since index starts from 0
    const currentPage = pageData.pageIndex + 1;
    console.log('current page -', currentPage);
    const pageSize = pageData.pageSize;
    console.log('cuurent pageSize - ', pageSize);
    console.log('pagination event - ', pageData);
    // fetching after changing pagination
    this.authService.getPosts(pageSize, currentPage).subscribe(data => {
      this.posts = data.posts;
      this.pageLength = data.totalPosts;
    });
  }
}
