import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tokenNotExpired } from 'angular2-jwt';
import { responseFromApi } from 'interface';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
authToken: any;
user: any;
id: any;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  // register
  addUser(data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<responseFromApi>('/users/register', data, {headers: headers});
  }

  // authenticate user
  authenticateUser(data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<responseFromApi>('/users/authenticate', data, {headers: headers});
  }

  // get user
  getUser() {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get<responseFromApi>('/users/profile', {headers: headers});
  }

  // edit userinfo
  editUserInfo(id, newData) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = `/users/profile/${id}`;
    return this.http.put<responseFromApi>(url, newData, {headers: headers});
  }

  // reset password method
  resetPassword(email) {
    const url =  `/users/forgot`;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<responseFromApi>(url, email, {headers: headers});
  }

  // update password
  updatePassword(newPassword) {
    const url = '/users/update-password';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.put<responseFromApi>(url, newPassword, {headers: headers});
  }

  // on email click link
  resetPasswordLinkClick() {
    const token = JSON.parse(localStorage.getItem('resetToken'));
    const resetTime = JSON.parse(localStorage.getItem('resetTokenTime'));
    const url = `/users/reset/${token}`;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get<responseFromApi>(url, {headers: headers});
  }

  // create post
  createPost(data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = '/posts/add';
    return this.http.post<responseFromApi>(url, data, {headers: headers});
  }

  // get all posts
  getPosts(bookPerPage: number, currentPage: number ) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const queryParameters = `?pageSize=${bookPerPage}&page=${currentPage}`;
    return this.http.get<responseFromApi>('/posts/' + queryParameters);
  }

  // get post by id
  getPostById(id) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = '/posts/' + id;
    return this.http.get<responseFromApi>(url);
  }

  // edit post
  editPost(id, data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = '/posts/edit/' + id;
    return this.http.put<responseFromApi>(url, data, {headers: headers});
  }

  // delete post
  deletePostById(id) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = '/posts/delete/' + id;
    return this.http.delete<responseFromApi>(url);
  }

  // method to subscribe to the blog
  subscribeNewsLetter(email) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = '/posts/subscribe';
    return this.http.post<responseFromApi>(url, email, {headers: headers});
  }


  // function to get user from local storage
  getUsersLocalStorage() {
    this.user = JSON.parse(localStorage.getItem('user'));
    // this.id = this.user.id;
    // console.log()
    return this.user;
  }

  // function to store the data
  storeUserData(token, user) {
    // saving data into local storage
    localStorage.setItem('id_token', token);
    // localstorage can only store strings so convert object into strings
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
    this.id = user.id;
  }

  // function to logout
  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
    return false;
  }
  // function to check logged in
  loggedIn() {
    return tokenNotExpired('id_token');
  }

  // function to load token from local storage
  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
    const user = localStorage.getItem('user');
    this.user = JSON.parse(user);
  }

  // functio to add comment
  addComment(id, data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    const url = `/posts/comment/${id}`;
    return this.http.post<responseFromApi>(url, data, {headers: headers});
  }

  // fuunction to likes
  likes(id) {
    const url = `/posts/likes/${id}`;
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.put<responseFromApi>(url, {headers: headers});
  }
  // function to add dislikes
  dislikes(id) {
    const url = `/posts/dislikes/${id}`;
    const headers = new HttpHeaders();
    headers.append('Conntent-type', 'application/json');
    return this.http.put<responseFromApi>(url, {headers: headers});
  }
}
