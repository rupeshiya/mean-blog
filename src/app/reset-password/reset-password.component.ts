import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
email: any;
  constructor(
    private authService: AuthServiceService,
    private flashMessage: FlashMessagesService,
    private router: Router
    ) { }

  ngOnInit() {
  }

  // method for reseting password
  onResetClick() {
    const email = {
      email: this.email
    };
    console.log('email for reset password is - ', this.email);
    this.authService.resetPassword(email).subscribe((res) => {
      if (res.success) {
        console.log('email sent successfully !!');
        this.router.navigate(['/login']);
        this.flashMessage.show('Reset link has been sent to your email , please check your email !!', {cssClass: 'alert-success', timeout: 5000});
      } else {
        console.log('error in sending the email !!');
        this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }

}
