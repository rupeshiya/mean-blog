import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
username: any;
name: any;
email: any;
userID: any;

  constructor(
  private authService: AuthServiceService,
  private router: Router,
  private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo() {
   const userInfo = this.authService.getUsersLocalStorage();
  //  this.userInfo = JSON.parse(userInfo);
    console.log(userInfo);
    this.username = userInfo.username;
    this.name = userInfo.name;
    this.email = userInfo.email;
    this.userID = userInfo.id;
  }
  // on edit
  onEditButton() {
    this.router.navigate([`/editProfile/${this.userID}`]);
  }

}
