import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
email: any;

  constructor(
    private router: Router,
    private authService: AuthServiceService,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  onSubscribeClick() {
    const email = {
      email: this.email
    };
    this.authService.subscribeNewsLetter(email).subscribe((res) => {
      if (res.success) {
        console.log('successfully subscribed to the newsletter');
        this.router.navigate(['/allPosts']);
        this.flashMessage.show('Thanks for subscribing !!', {cssClass: 'alert-success', timeout: 3000});
      } else {
        console.log('Something went wrong !!');
        this.flashMessage.show(`${res.msg}`, {cssClass: 'alert-danger', timeout: 3000});
      }
    });
  }
}
