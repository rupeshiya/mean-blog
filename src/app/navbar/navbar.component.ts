import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    public authService: AuthServiceService,
    private flashMessage: FlashMessagesService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  // on logout click
  onLogoutClick() {
    this.authService.logout();
    this.flashMessage.show('Sucessfully logged out !!', {cssClass: 'alert-success', timeout: 3000});
    this.router.navigate(['/login']);
  }

}
