import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
username: any;
name: any;
email: any;
password: any;
confirmPassword: any;
// errors: any = [];

  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) { }

  ngOnInit() {
  }

  // for signup
  onSignUpClick() {
    const info = {
      username: this.username,
      name: this.name,
      email: this.email,
      password: this.password
      // confirmPassword: this.confirmPassword
    };
    if (this.validateForm()) {
      this.passwordConfirm();
      this.authService.addUser(info).subscribe((res) => {
        if (res.success) {
          console.log('successfully added the user');
          this.flashMessage.show('Successfully registered !!', {cssClass: 'alert-success', timeout: 2000 });
          this.router.navigate(['/login']);
        } else {
          this.flashMessage.show(`${res.msg}`, {cssClass: 'alert-danger', timeout: 3000});
        }
      });
    }
  }

  // confirmPassword function
  passwordConfirm() {
    if (!(this.password === this.confirmPassword)) {
      // this.errors.push({error: 'Password do not match !!'});
      this.flashMessage.show('Password don\'t match !!', {cssClass: 'alert-danger', timeout: 3000});
    }
  }
  // validate Login Form
  validateForm(): boolean {
    if (this.username === undefined) {
      this.flashMessage.show('Please enter the username !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    } else if (this.email === undefined) {
      this.flashMessage.show('Please enter the email !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    } else if (this.password === undefined) {
      this.flashMessage.show('Please enter the password !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    } else if (this.name === undefined ) {
      this.flashMessage.show('Please enter the name !!', {cssClass: 'alert-success', timeout: 3000});
      return false;
    }
    return true;
  }

}
