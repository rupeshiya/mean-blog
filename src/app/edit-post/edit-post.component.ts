import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
id: any;
content: any;
title: any;

  constructor(
    private authService: AuthServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private flashMessage: FlashMessagesService
    ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params.id;
    });
  }

  // on submit
  onEditDone() {
    const newData = {
      title: this.title,
      content: this.content
    };
    if (this.validatePost()) {
      this.authService.editPost(this.id, newData).subscribe((res) => {
        if (res.success) {
          console.log('edited the post !!');
          this.router.navigate(['/allPosts']);
        } else {
          console.log('error in editing !!');
          this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
        }
      });
    }
  }

  // validate post
  validatePost(): boolean {
    if (this.title === undefined) {
      this.flashMessage.show('Please enter title !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    } else if (this.content === undefined) {
      this.flashMessage.show('Please enter description of the post !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }
   return true;
  }
}
