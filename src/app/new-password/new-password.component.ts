import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../services/auth-service.service';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {
confirmPassword: any;
newPassword: any;

  constructor(
    private router: Router,
    private authService: AuthServiceService
    ) { }

  ngOnInit() {
  }

  // on submit of new password
  onSubmitClick() {
    // this.authService.resetPasswordLinkClick()
  }

}
