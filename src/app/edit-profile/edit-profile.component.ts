import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
userInfo: any;
name: any;
username: any;
email: any;
userID: any;
id: any;

  constructor(
    private authService: AuthServiceService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params.id;
    });
    this.getUserInfo();
  }

  getUserInfo() {
    const userInfo = this.authService.getUsersLocalStorage();
   //  this.userInfo = JSON.parse(userInfo);
     console.log(userInfo);
     this.username = userInfo.username;
     this.name = userInfo.name;
     this.email = userInfo.email;
   }

  // edit userifo
  onEditDone() {
    const newData = {
      name: this.name,
      username: this.username,
      email: this.email
    };
    if (this.validateProfile()) {
      this.authService.editUserInfo(this.id, newData).subscribe((res) => {
        if (res.success) {
          console.log('Successfully edited the user info !!', {cssClass: 'alert-success', timeout: 2000});
          this.router.navigate(['/profile']);
        } else {
          this.flashMessage.show('Something went wrong !!', {cssClass: 'alert-danger', timeout: 3000});
        }
      });
    }
  }

  // check for validation
  validateProfile(): boolean {
    if (this.name === undefined) {
      this.flashMessage.show('Please enter the name !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    } else if (this.email === undefined) {
      this.flashMessage.show('Please enter the email !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    } else if (this.username === undefined) {
      this.flashMessage.show('Please enter the username !!', {cssClass: 'alert-danger', timeout: 3000});
      return false;
    }
    return true;
  }

}
