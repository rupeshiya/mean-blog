export interface responseFromApi {
  success?: boolean;
  msg?: String;
  posts?: String;
  post?: String;
  err?: String;
  totalPosts?: number;
  token?: any;
  user?: any;
  totalUpVote?: any;
  totalDownVote?: any;
}
