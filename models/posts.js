const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
    title:{
        type: String
    },
    body:{
        type: String
    },
    date:{
        type: Date,
        default: Date.now()
    },
    comment: {
        type: String
    },
    postedBy:{
        type: String
    },
    upVote:{
        type: Number,
        default: 0
    },
    downVote:{
        type: Number,
        default: 0
    }
    // user:{
    //   type: mongoose.SchemaTypes.ObjectId, ref:'User'
    // }
});

module.exports = mongoose.model('Posts',postSchema);