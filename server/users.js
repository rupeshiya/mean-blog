const express = require('express');
const router = express.Router();
const User = require('../models/users');
const jwt = require('jsonwebtoken');
// const keys = require('../config/keys');
const async = require('async');
const crypto = require('crypto');
const { mailOptions ,sendMailOnRegister } = require('./mail');
const path = require('path');


// =========================== REGISTER =========================================== //
// ================================================================================ //

// /users/register route
router.post('/register',(req,res)=>{
    const username = req.body.username;
    User.getUserByUsername(username,(err,user)=>{
        if(err){
            console.log('error in finding user !!');
            res.status(500).json({success: false, msg: 'error in server '});
        }
        if(user){
            console.log('user already exists !!');
            res.status(200).json({success: false, msg: 'User alreday exits !!'});
        }
        if(!user){
        // if user not exists then create
            const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
                username: req.body.username
            });
            console.log('newUser -',newUser);
            User.addUser(newUser, (err,user)=>{
                if(err){
                    res.status(500).json({success: false, msg: 'unable to register'});
                } else {
                    console.log('user -',user);
                    mailOptions.to = req.body.email;
                    mailOptions.subject = 'Successfully registered !!';
                    mailOptions.text = 'Thanks for registering yourself on MeanBlog \n\n' + 'Welcome to MeanBlog !! \n\n' + 'Happy reading !!';
                    sendMailOnRegister();
                    res.status(200).json({success: true, msg: 'Successfully registered !!'});
                }
            });
        }
    });
});

// =============================== AUTHENTICATION ================================== //
// ================================================================================= //

// /users/authenticate route
router.post('/authenticate',(req,res)=>{
    // res.send('auth works!!!');
    const username =  req.body.username;
    const password =  req.body.password;
    User.getUserByUsername(username,(err,user)=>{
        if(err){
            console.log('error in username ',err);
        }
        if(!user){
            return res.json({success: false,msg:'user does not exists'});
        } 
        // compare password for authentication
        // user.password is hashed password
        User.comparePassword(password, user.password,(err,isMatch)=>{
            if(err){
                console.log('error in users.js ',err);
            }
            if(isMatch){
                // making token
                const token = jwt.sign({data: user}, `${process.env.secret}`, {
                    expiresIn: 604800 // 1 week
                });
                // sending response to the front-end
                res.json({
                    success: true,
                    token: 'JWT '+token,
                    user: {
                        id: user._id,
                        name: user.name,
                        username: user.username,
                        email: user.email,
                        role: user.role
                    }
                });
            } else {
                return res.json({success: false, msg: 'Wrong password'});
            }
        });
    });
});


// /users/profile route
router.get('/profile',(req,res)=>{
    // res.send('profile works!!');
    console.log('user info-',req.user);
    res.status(200).json({success: true,user: req.user});
});

// update user profile
router.put('/profile/:id',(req,res)=>{
  
    User.findById(req.params.id)
        .then((user)=>{
            user.name = req.body.name;
            user.username = req.body.username;
            user.email = req.body.email;
            // user.password = req.body.password;
            user.save()
                .then(()=>{
                    console.log('msg from server -updated profile');
                    res.status(200).json({success: true, msg:'updated the user'});
                })
                .catch((err)=>{
                    console.log('msg from server -not updated profile');
                    res.status(500).json({success: false, msg:'unable to update the users profile',err: err});
                });
        })
        .catch((err)=>{
            console.log('msg from server -not updated profile');
            res.status(500).json({success: false, msg: 'unable to update the profile',err: err});
        });
});


// routes for forgot password /users/forgot
router.post('/forgot',(req,res)=>{
    async.waterfall([
        function(done){
            // creating random tokens using crypto 
            crypto.randomBytes(20,(err,buf)=>{
                const token = buf.toString('hex');
                done(err,token);
                console.log('token after creating token - ', token);
            });
        },
        function(token, done){
            // checking user if exists or not
            console.log('email from the server  for reseting password is - ', req.body.email);
            User.findOne({email: req.body.email })
                .then((user)=>{
                    console.log('user from reset -',user);
                    user.resetPasswordToken = token;
                    user.resetPasswordTime = Date.now() + 3600000 ; // 1 hr
                    user.save((err)=>{
                        done(err,token,user);
                    });
                })
                .catch((err)=>{
                    if(err){
                        console.log('error in the server side for reseting the password !!',err);
                        res.status(500).json({success: false, msg: 'error in the server'});
                        done(err);
                    }
                });
        },
        function(token, user, done){
            req.query.params = token;
            mailOptions.to = user.email;
            mailOptions.subject = 'Node.js Password Reset';
            mailOptions.text = 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
      'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
      'http://' + req.headers.host + '/users/reset/?' + req.query.params + '\n\n' +
      'If you did not request this, please ignore this email and your password will remain unchanged.\n';
            //http://localhost:4000/users/reset/?:token sent in email
            sendMailOnRegister();
            res.status(200).json({success: true, msg:'successfully sent the reset password link to the mail '});
            // done();
        }
    ],(err)=>{
        console.log('error in the async waterfall server side !!',err);
    });
});

// routes to handle the sent email link /users/reset/:token
router.get('/reset/:token',(req,res)=>{
    const resetPasswordToken = req.params.token;
    // checking 
    User.findOne({ resetPasswordToken: resetPasswordToken , resetPasswordTime:{ $gt:Date.now() } },(err, user)=>{
        if(err){
            console.log('error in server finding the token !!',err);
            res.status(400).json({success: false, msg:'Server error in finding the token !!'});
        } 
        if(!user){
            console.log('user not found !!');
            res.status(500).json({success: false, msg: 'unable to find the user !!'});
        }
        res.sendFile(path.join(__dirname,'../dist/mean-blog/assets','reset.html'));
    // res.status(200).json({success:true, msg:'found the user with the valid reset token', user:user});
    });
});

// post method to save new password
router.post('/reset/:token',(req,res)=>{
    async.waterfall([
        function(done){
            User.findOne({resetPasswordToken: req.params.token, resetPasswordTime: {$gt: Date.now()} },(err,user)=>{
                if(err){
                    console.log('error in the server side for finding the user !!');
                    res.status(500).json({success: false, msg: 'Error in the server' });
                }
                if(!user){
                    console.log('User not found !!');
                    res.status(400).json({success: false, msg: 'Unable to find the user !!'});
                }
                user.password = req.body.password;
                user.resetPasswordToken = null;
                user.resetPasswordTime = null;
                user.save().then((err)=>{
                    console.log('error in saving user !!');
                });
                done(err,user);
            });
        },
        function(user,done,err){
            mailOptions.to = user.email;
            mailOptions.subject = 'Your password has been changed';
            mailOptions.text = 'Hello,\n\n' +
      'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n';
            sendMailOnRegister();
            done(err);
        }
    ],(err)=>{
        console.log('error in async waterfall !!');
    });
});

// update password routes /users/update-password
router.put('/update-password',(req,res)=>{
    User.findOneAndUpdate({username: req.body.username})
        .then((user)=>{
            user.password = req.body.newPassword;
            res.status(200).json({success: true, msg:'successfully updated the password !!'});
        })
        .catch((err)=>{
            console.log('error in updating the password !!');
            res.status(500).json({success: false, msg:'unable to update the password !!'});
        });
});

// implemnting the folow users functionality  /follow/:id

router.get('/follow/:id',(req,res)=>{
    User.findById(req.params.id)
        .then((user)=>{
            const followerName = escape(req.query.name);
            user.followers.push({name: followerName, _id: req.query.id});
            user.save()
            .then(()=>{
                res.status(200).json({success: true, msg: 'successfully followed !!'});
            })
            .catch((err)=>{
                console.log('error in following ',err);
                res.status(500).json({success: false, msg:`${err}`});
            });
        })
        .catch((err)=>{
            console.log('error in the server !!',err);
            res.status(500).json({success: false, msg: `${err}`});
        });
});


//  following user functionality /following/:id

router.get('/following/:id',(req,res)=>{
    User.findById(req.params.id)
        .then((user)=>{
            const followingName = escape(req.query.name);
            user.following.push({name:followingName, _id: req.query.id});
            user.save()
            .then((user)=>{
                res.status(200).json({success: true, msg: 'following data saved !!'});
            })
            .catch((err)=>{
                console.log('error', `${err}`);
            });
        })
        .catch((err)=>{
            console.log('error ',`${err}`);
            res.status(500).json({success: false, msg:`${err}`});
        });
});

// blocking user /block/:id
router.get('/block/:id',(req,res)=>{
    User.findById(req.params.id)
        .then((user)=>{
            user.following.filter((user)=> user.following._id !== req.params.id )
            .save()
            .then(()=>{
                console.log('removed from the following list!!');
                res.status(200).json({success: true, msg:'successfully removed from saved following db'});
            })
            .catch((err)=>{
                console.log('error in db',`${err}`);
            });
        })
        .catch((err)=>{
            console.log('error in db catch',`${err}`);
        });
});

// functionality to check if following or not
// weh have to store all the following usr data ian array ad call everytime for the calling is following method on it.

router.get('/isfollowing/:id',(req,res)=>{
    User.findById(req.params.id)
        .then((user)=>{
            user.following.forEach(user => {
                if(user._id == req.params.id){
                    res.status(200).json({success: true, following: true, msg:'following'});
                } else{
                    res.status(500).json({success: false, following: false, msg: 'not following'});
                }
            });
        })
        .catch((err)=>{
            res.status(500).json({success: false, msg: `${err}`});
        });
});






module.exports = router;