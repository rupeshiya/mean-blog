const express = require('express');
const router = express.Router();
const Posts = require('../models/posts');
const { mailOptions ,sendMailOnRegister , scheduledEmail } = require('../server/mail');
const cron = require('node-cron');

// routes for posting the post
router.post('/add',(req,res)=>{
    const newPost = {
        title: req.body.title,
        body: req.body.content,
        date: Date.now()
    };
  
    new Posts(newPost)
        .save()
        .then((data)=>{
            console.log('post saved');
            res.status(200).json({success: true, msg: 'posts saved'});
        })
        .catch((err)=>{
            console.log('unnable to save the posts',err);
        });
});

// routes for deleting the post
router.get('/',(req,res)=>{
    var postsFetched = null;
    const pageSize = +req.query.pageSize;
    const currentPage = +req.query.page;
    const query = Posts.find({});
    if(pageSize && currentPage){
        query
            .skip(pageSize *(currentPage - 1))
            .limit(pageSize);
    }
    query
    // .sort({date: 'desc'})
        .then((posts)=>{
            console.log(posts);
            postsFetched = posts;
            console.log('post fetched a-',postsFetched);
            return Posts.estimatedDocumentCount();
        })
        .then((count)=>{
            // console.log('post fetched b-',postsFetched);
            res.status(200).json({ success: true, msg:'succesfully fetched posts!!',posts: postsFetched, totalPosts : count });
        })
        .catch((err)=>{
            console.log('error in fetching posts !!',err);
            res.status(500).json({success: false,msg:'unable to fetch posts !!',err: err});
        });
});

// get post by id
router.get('/:id',(req,res)=>{
    Posts.findById(req.params.id).lean()
        .then((post)=>{
            res.status(200).json({success: true, msg: 'sending the post data',post: post});
        })
        .catch((err)=>{
            res.status(500).json({success: false, msg: 'unable to find the post data',err: err});
        });
});

// routes for updating the post
router.put('/edit/:id',(req,res)=>{
    Posts.findById(req.params.id)
        .then((post)=>{
            post.title = req.body.title;
            post.body = req.body.content;
            post.save()
                .then(()=>{
                    res.status(200).json({success: true, msg: 'updated the post'});
                })
                .catch((err)=>{
                    res.status(500).json({success: false, msg: 'unable to update the post'});
                });
        })
        .catch((err)=>{
            console.log('error in editing the post');
            res.status(200).json({success: true, msg: 'error in updating the post'});
        });
});

//delete post
router.delete('/delete/:id',(req,res)=>{
    Posts.remove({_id: req.params.id})
        .then((post)=>{
            res.status(200).json({success: true, msg: 'successfully deleted the post'});
        })
        .catch((err)=>{
            res.status(500).json({success: false, msg: 'unable to delete the post'});
        });
});

router.post('/comment/:id',(req,res)=>{

    Posts.findById(req.params.id)
        .then((post)=>{
            post.title = req.body.title;
            post.comment = req.body.commentBody;

            post.save()
                .then((res)=>{
                    res.status(200).json({success: true, msg:'successfully saved the comment !!'});
                })
                .catch((err)=>{
                    res.status(500).json({success: false, msg: 'unable to save the comment !!',err:err});
                });
        })

        .catch((err)=>{
            res.status(500).json({success: false, msg: 'unable to save the comment !!',err:err});
        });
    
});

// for adding likes and dislikes 
router.put('/dislikes/:id',(req,res)=>{
    Posts.findByIdAndUpdate({_id: req.params.id},{$inc:{downVote: 1}}, {new: true},(err,post)=>{
        if(err){
            console.log('error in disliking from the server !!');
            res.status(500).json({success: false, msg: 'Unable to dislike the post !!',err});
        } 
        if(post) {
            console.log('Successfully disliked the post !!');
            res.status(200).json({success: true, msg: 'Successfully disliked the posts !!'});
        }
    });
});

router.put('/likes/:id',(req,res)=>{
    Posts.findByIdAndUpdate({_id: req.params.id },{$inc:{upVote: 1}},{new: true}, (err, post)=>{
        if(err){
            console.log('error in liking from the server !!');
            res.status(500).json({success: false, msg: 'Unable to like the post !!',err});
        } 
        if(post) {
            console.log('Successfully liked the post !!');
            res.status(200).json({success: true, msg: 'Successfully liked the posts !!',totalUpVote: post.upVote , totalDownVote: post.downVote });
        }
    });
});


// ================================== FOR SUBSCRIBING TO THE NEWSLETTER ================== //
// ======================================================================================= //
require('../models/subscribe');
const Subscriber = require('../models/subscribe');
// route is /posts/subscribe
router.post('/subscribe',(req,res)=>{
    Subscriber.findOne({email: req.body.email})
        .then((subscriber)=>{
            if(!subscriber){
                const subscriberEmail = {
                    email: req.body.email
                };
                new Subscriber(subscriberEmail).save()
                    .then((subscriber)=>{
                        mailOptions.to = req.body.email;
                        mailOptions.subject = 'Newsletter subscription !!';
                        mailOptions.text = 'Thanks for subscribing our blog !!\n\n' +'Hope you are enjoying our post !!\n' +'We will update you with our latest blogs\n\n' + 'Happy blogging !!';
                        sendMailOnRegister();
                        scheduledEmail();
                        res.status(200).json({success: true, msg:'saved the subscriber email'});
                    })
                    .catch((err)=>{
                        console.log('error in the saving subscriber info !!',err);
                        res.status(500).json({success: false, msg:'error on saving subscriber info !!'});
                    });
            } else {
                console.log('user already subscribed !!',subscriber);
                res.status(200).json({success: false, msg:'Already subscribed !!',email: subscriber });
            }
        })
        .catch((err)=>{
            console.log('error in server !!',err);
            res.status(500).json({success: false, msg: 'Server error !!',err: err});
        });
    
});

module.exports = router;