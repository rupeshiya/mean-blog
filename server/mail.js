const mongoose = require('mongoose');
const express = require('express');
const router = express.Router;
const nodemailer = require('nodemailer');
const cron = require('node-cron');
const Posts = require('../models/posts');
require('dotenv').config();

const transporter = nodemailer.createTransport({
    service: `${process.env.mailService}`,
    auth:{
        user: `${process.env.sendgridUsername}`,
        pass: `${process.env.sendgridPassword}`
    }
});

const mailOptions = {
    to: '',
    from: `${process.env.emailFrom}`,
    subject: '',
    text: ''
};

const sendMailOnRegister = () =>{
    transporter.sendMail(mailOptions,(err)=>{
        if(err){
            console.log('error in sending the mail !!');
        }
        console.log('successfully sent the mail !!');
    });
};

// scheduling tasks to send monthly posts
const scheduledEmail = ()=> { cron.schedule('59 23 2 1-12 sat',()=>{
    console.log('sending the posts everymonth on saturday');
    Posts.find({}).lean()
        .then((posts)=>{
            // mailOptions.to = '';
            mailOptions.subject = 'Monthly posts !!';
            var message = [];
            posts.forEach(post => {
                message.push(post.title);
            });
            // sending top 5 posts
            for(var i = 0; i <= 5; i++){
                mailOptions.text = `
                <h3>Monthly Interesting Posts !!</h3>
                <ul>
                    <li>'\n' + ${message[i].text} + '\n'</li><br>
                </ul>
                `;
            }
            // mailOptions.text = `${message[].title}`
            sendMailOnRegister();
        });
});
};

module.exports = {
    transporter,
    mailOptions,
    sendMailOnRegister,
    scheduledEmail
};