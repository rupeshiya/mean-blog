const express = require('express');
const mongoose = require('mongoose');
const port = process.env.PORT || 4000;
const app = express();
const bodyPraser = require('body-parser');
const cors = require('cors');
const path = require('path');
const logger = require('morgan');
require('dotenv').config();
const cron = require('node-cron');

// middlewares
app.use(cors());
app.use(bodyPraser.json());
app.use(bodyPraser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname,'dist/mean-blog')));
app.use(logger('combined'));

// connect mongoose
// const keys = require('./config/keys');
mongoose.connect(`${process.env.mongoUri}`,{useNewUrlParser: true})
    .then(()=>{
        console.log('mongo connected');
    })
    .catch((err)=>{
        console.log('error in mongoose conncetion -',err);
    });

// routes
app.get('/',(req,res)=>{
    // res.status(200).json({success: true, msg: '/ works'});
    res.sendFile(path.join(__dirname,'dist/mean-blog/index.html'));
    cron.schedule('1,2,4,5 * * * *', () => {
        console.log('running every minute 1, 2, 4 and 5');
    });
});

app.get('/test',(req,res)=>{
    res.status(200).json({success: true});
});



// 
const users = require('./server/users');
app.use('/users',users);
const posts = require('./server/posts');
app.use('/posts',posts);


app.listen(port,()=>{
    console.log(`listening on ${port}`);
});